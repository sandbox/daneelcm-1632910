<?php
/**
 * Created by PhpStorm.
 * User: danee
 * Date: 8/16/2016
 * Time: 3:50 PM
 */

namespace Drupal\dbconnect\Form;

use Drupal\Core\Database;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class dbconnectSettingsForm extends ConfigFormBase {

  /**
   * @inheritdoc
   */
  protected function getEditableConfigNames() {
    return ['dbconnect.settings'];
  }

  /**
   * @inheritdoc
   */
  public function getFormId() {
    return 'dbconnect_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('dbconnect.settings');
    $form['target_multiple'] = array(
      '#type' => 'select',
      '#title' => t('Target databases behavior'),
      '#options' => array(
        'user' => t('User selection'),
        'fixed' => t('Fixed target database'),
      ),
      '#description' => t('Target database may be selected by users at login or use only one for the whole site.'),
      '#default_value' => $config->get('target_multiple'),
    );
    $databases = Database\Database::getAllConnectionInfo();
    foreach ($databases['default'] as $target => $val) {
      if (isset($val['description']))
        $opts[$target] = $target . ' - ' . $val['description'];
    }
    $form['fixed'] = array(
      '#type' => 'select',
      '#title' => t('Fixed database'),
      '#default_value' => $config->get('fixed'),
      '#options' => $opts,
      '#states' => array(
        'visible' => array(
          ':input[name="target_multiple"]' => array('value' => 'fixed'),
        ),
      ),
    );
    $form['target_mandatory'] = array(
      '#type' => 'checkbox',
      '#title' => t('User must select a Target Database at login.'),
      '#default_value' => $config->get('target_mandatory'),
      '#states' => array(
        'visible' => array(
          ':input[name="target_multiple"]' => array('value' => 'user'),
        ),
      ),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('dbconnect.settings')
      ->set('target_multiple', $form_state->getValue('target_multiple'))
      ->set('target_mandatory', $form_state->getValue('target_mandatory'))
      ->set('fixed', $form_state->getValue('fixed'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}